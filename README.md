# NSS Semestral project
----
By Aziza Kuspekova
## Technologies Used
### Back end
- Java
- Jsoup
### Front end
- React
- MUI
## Description
The project's goal is to design and implement an application which will analyze world university rankings.
## Setup/Installation Requierements
- Clone this repository to the desktop
- Navigate to the top level of the directory.
- Open README.md
----
&copy; KUSPEAZI
